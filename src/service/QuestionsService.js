import config from '../config'

const service = {
  async getQuestion (guid) {
    const url = new URL(`${config.url}/questions`)
    url.search = new URLSearchParams({ userGuid: guid }).toString()
    const response = await fetch(url.toString())
    return await response.json()
  },
  async sendAnswer (userGuid, questionGuid, answerGuid) {
    const response = await fetch(`${config.url}/questions`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        userGuid: userGuid,
        questionGuid: questionGuid,
        answerGuid: answerGuid
      })
    })
    return await response.json()
  }
}

export default service
