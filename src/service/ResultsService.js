import config from '../config'

const service = {
  async getResults (guid) {
    const url = new URL(`${config.url}/results`)
    url.search = new URLSearchParams({ userGuid: guid }).toString()
    const response = await fetch(url.toString())
    return await response.json()
  }
}

export default service
