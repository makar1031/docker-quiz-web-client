import config from '../config'

const service = {
  getGuid () {
    return localStorage.userGuid
  },
  async createUser (username) {
    const response = await fetch(`${config.url}/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(username)
    })
    const body = await response.json()
    localStorage.userGuid = body.guid
    return true
  },
  async removeUser (guid) {
    localStorage.removeItem('userGuid')
    const url = new URL(`${config.url}/users`)
    url.search = new URLSearchParams({ guid }).toString()
    await fetch(url.toString(), {
      method: 'DELETE'
    })
  }
}

export default service
