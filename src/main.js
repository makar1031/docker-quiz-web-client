import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Notifications from 'vue-notification'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Notifications)

Vue.config.errorHandler = function (err, vm) {
  console.log(err)
  vm.$notify({
    group: 'global',
    type: 'error',
    title: 'Произошла ошибка',
    text: err.toString()
  })
}

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
